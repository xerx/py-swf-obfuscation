class Math:
	@staticmethod
	def add(*args):
		sum = 0
		for number in args:sum += number
		return sum
	@staticmethod
	def multiply(*args):
		product = 1
		for number in args:product *= number
		return product

