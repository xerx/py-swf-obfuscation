from json import JSONEncoder
import shutil
import os

#########################################  FolderMapper
class Encoder(JSONEncoder):
	def default(self, o):
		return o.__dict__

class Item:
	def __init__(self, name, contents = None):
		self.name = name
		if(contents != None):
			self.contents = contents

class FolderMapper:
	def __init__(self, path):
		self.path = path
		self.map = Item("root", self.searchInFolder(path))
		self.map_json = Encoder().encode(self.map)
	def searchInFolder(self, path):
		folder_contents = os.listdir(path)
		items = []
		for item_name in folder_contents:
			item_path = path + "\\" + item_name
			if(os.path.isdir(item_path)):
				item = Item(item_name, self.searchInFolder(item_path + "\\"))
			else:
				item = Item(item_name)
			items.append(item)
		return items
	def getLeafPathList(self, prefix):
		paths_list = []
		self.addLeafPathsToList(self.map, "", paths_list)
		for i in range(len(paths_list)):
			paths_list[i] = prefix + paths_list[i]
		return paths_list
	def addLeafPathsToList(self, item, leaf_path, leafs_list):
		if(hasattr(item, "contents")):
			is_leaf = True
			for i in range(len(item.contents)):
				if(hasattr(item.contents[i], "contents")):
					is_leaf = False
					path_arg = "" if item.name == "root" else leaf_path + "\\" + item.name 
					self.addLeafPathsToList(item.contents[i], path_arg, leafs_list)
			if(is_leaf):
				leafs_list.append(leaf_path + "\\" + item.name)	
	def getFilesList(self, prefix):
		files_list = []
		self.addFilesToList(self.map, "", files_list)
		for i in range(len(files_list)):
			files_list[i] = prefix + files_list[i]
		return files_list
	def addFilesToList(self, item, file_path, files_list):
		if(hasattr(item, "contents")):
			for i in range(len(item.contents)):
				path_arg = "" if item.name == "root" else file_path + "\\" + item.name 
				self.addFilesToList(item.contents[i], path_arg, files_list)
		else:
			files_list.append(file_path + "\\" + item.name)	
	def save(self):
		os.chdir(self.path)
		map_file = open("folder_map.json", "w")
		map_file.write(self.map_json)
		map_file.close()
#########################################  FolderMapper

#########################################  FileManager

class FileManager:
	def copyFiles(self, files_list, root_source, root_destination):
		for i in range(len(files_list)):
			path = files_list[i].replace(root_source, root_destination)
			print("[~] Copying " + str(i + 1) + " out of " + str(len(files_list)) + " files", end="\r")
			try:
				shutil.copy(files_list[i], path)
			except shutil.Error as e:
				print("\nError copying: " + files_list[i] + "\n")
			except IOError as e:
				print("\nError copying: " + files_list[i] + "\n")
		print("\n[*] Copy complete!")
	def makeDirectoryTrees(self, paths_list):
		print("[~] Creating directories")
		for i in range(len(paths_list)):
			if(not(os.path.exists(paths_list[i]))):
				os.makedirs(paths_list[i])
		print("[*] Directories created!")
	def deleteEmptyDirectories(self, root_dir):
		dirs = os.listdir(root_dir)
		if(dirs == []):
			os.rmdir(root_dir)
		else:
			for i in range(len(dirs)):
				if(os.path.isdir(root_dir + "\\" + dirs[i])):
					self.deleteEmptyDirectories(root_dir + "\\" + dirs[i])
			if(os.listdir(root_dir) == []):
				os.rmdir(root_dir)	
	def saveFile(self, data, path, mode = "w+"):
		file_to_save = open(path, mode)
		file_to_save.write(data)
		file_to_save.close()
		print("[*] File {0} saved!".format(path))
	
#########################################  FileManager