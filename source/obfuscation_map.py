import re
import os
import random

class ObfuscationMapCreator:
    static_file_name = "replace_map.txt"

    def __init__(self):
        self.REPLACEMENT_CHARS = [";", "^", "!", "@", "#", "$", "%", "&", "*", "|", "+", "-", "?", "~"]
        self.REPLACEMENT_NUMS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        self.code_root = ""
        self.package_list = []
        self.class_name_list = []
        self.random_strs_list = []
        self.func_names_list = []
        self.forbidden = self.getForbiddenList()
    
    def getForbiddenList(self):
        forbidden_list = []
        try:
            forbidden_file = open("FORBIDDEN.txt")
            lines = forbidden_file.readlines()
            for line in lines:
                forbidden_list.append(line.replace("\n", ""))
        except FileNotFoundError:
            print("[*] Using internal forbidden list")
            forbidden_list = FORBIDDEN
        return forbidden_list

    def create(self, code_root_path):
        print("[~] Creating map...")
        self.code_root = code_root_path
        self.collectClasses(code_root_path)
        self.finalize()
        return self.code_root + "\\" + ObfuscationMapCreator.static_file_name

    def collectClasses(self, path, prefix = ""):
        folder_contents = os.listdir(path)
        for item_name in folder_contents:
            item_path = path + "\\" + item_name
            if(os.path.isdir(item_path)):
                self.collectClasses(item_path, prefix + item_name + r".")
            else:
                temp_path = path.replace(self.code_root + "\\", "")
                temp_path = temp_path.replace("\\", ".")
                temp_item_name = re.sub(r".as$", "", item_name)
                if(self.isUniqueItem(temp_path)):
                    self.package_list.append(temp_path)
                if(self.isUniqueItem(temp_item_name)):
                    self.class_name_list.append(temp_item_name)
                self.collectFunctions(item_path)

    def collectFunctions(self, path):
        as_file = open(path, "r", encoding="utf-8")
        lines = as_file.readlines()
        re_pattern = r"(static|final)?\s*(private|public|protected)\s*?(static|final)?\s*?(final|static)?\s*function\s*(get|set)?"
        for line in lines:
            if(len(line) > 4 and line.find("override") == -1 and re.search(re_pattern, line)):
                func_name = re.sub(re_pattern, "", line)
                func_name = re.search(r"\w+", func_name).group(0)
                if(len(func_name) > 3 and func_name[0].islower()):
                    if(self.isUniqueItem(func_name)):
                        self.func_names_list.append(func_name)

    def isUniqueItem(self, item_name):
        lists = [FORBIDDEN, self.package_list, self.class_name_list, self.func_names_list]
        for item in lists:
            isValid = False
            try:
                item.index(item_name)
            except ValueError:
                isValid = True
            if(not(isValid)):
                return False
        return isValid

    def getRandomReplacementStr(self, length, numeric = False):
        replacement_list = self.REPLACEMENT_NUMS if numeric else self.REPLACEMENT_CHARS
        random_str = ""
        while True:
            for i in range(0, length):
                index = random.randint(0, len(replacement_list) - 1)
                random_str += replacement_list[index]
            try:
                self.random_strs_list.index(random_str)
            except ValueError:
                break
        self.random_strs_list.append(random_str)
        return random_str   
    
    def addRandomReplacementsToList(self, some_list, numeric = False):
        for i in range(len(some_list)):
            random_str = self.getRandomReplacementStr(len(some_list[i]), numeric)
            some_list[i] = some_list[i] + " " + random_str

    def finalize(self):
        self.package_list.sort(key=lambda item:len(item), reverse=True)
        self.class_name_list.sort(key=lambda item:len(item), reverse=True)
        self.func_names_list.sort(key=lambda item:len(item), reverse=True)
        
        self.addRandomReplacementsToList(self.package_list)
        self.addRandomReplacementsToList(self.class_name_list, True)
        self.addRandomReplacementsToList(self.func_names_list, True)

        master_list = self.package_list + self.class_name_list + self.func_names_list
        list_file = open(self.code_root + "\\" + ObfuscationMapCreator.static_file_name, "w+", encoding="utf-8")
        list_file.write("\n".join(master_list))
        list_file.close()
        print("[*] Replace map created!")

#==================================================================

FORBIDDEN = [
"Dict",
"Key",
"OS",
"addFrameScript",
"apply",
"bitmapData",
"bounds",
"char",
"children",
"clear",
"click",
"clone",
"close",
"color",
"connect",
"contains",
"content",
"currentFrame",
"data",
"describeType",
"draw",
"duration",
"ease",
"easing",
"enable",
"extendsClass",
"field",
"flip",
"frame",
"height",
"implementsInterface",
"index",
"indexOf",
"init",
"interval",
"length",
"line",
"listener",
"load",
"log",
"media",
"move",
"moveTo",
"next",
"number",
"onMetaData",
"onPlayStatus",
"onXMPData",
"open",
"pause",
"play",
"position",
"properties",
"push",
"quit",
"remove",
"removeChild",
"select",
"size",
"sound",
"source",
"start",
"stop",
"stopAll",
"text",
"textFormat",
"time",
"timeout",
"tint",
"toString",
"trim",
"type",
"value",
"visible",
"volume",
"width"
]


