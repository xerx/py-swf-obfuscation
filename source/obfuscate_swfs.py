from obfuscation_map import ObfuscationMapCreator
from code.fileutils import FolderMapper
from code.fileutils import FileManager
import zlib
import os
import re

class Compressor:
    def compressSwfData(self, data):
        if(data[0:3] == b"CWS"):
            return data
        header = data[3:8]
        compressed_body = zlib.compress(data[8:len(data)])
        return b"CWS" + header + compressed_body
    def decompressSwfData(self, data):
        if(data[0:3] == b"FWS"):
            return data
        header = data[3:8]
        decompressed_body = zlib.decompress(data[8:len(data)])
        return b"FWS" + header + decompressed_body

#==================================================================================================

class Obfuscator:
    def __init__(self, replace_file_path):
        self.compressor = Compressor()
        #create replace dict
        replace_file = open(replace_file_path)
        replace_list = replace_file.readlines()
        self.replace_dict = dict()
        for line in replace_list:
            line = line.replace("\n", "")
            line_list = line.split(" ")
            self.replace_dict[line_list[0]] = line_list[1]
        self.sorted_dict_keys = sorted(iter(self.replace_dict.keys()), key = lambda name_key:len(name_key), reverse = True)

    def obfuscateSWF(self, path):
        print("[~] Obfuscating {0}".format(path, end="\r"))
        with open(path, "rb") as swf_file:
            data = swf_file.read()
        data = self.compressor.decompressSwfData(data)
        counter = 0
        keys_len = len(self.sorted_dict_keys)
        for key in self.sorted_dict_keys:
            counter += 1
            print("[~] Progress {0:.2f}".format(counter * 100 / keys_len) + " %", end="\r")
            if(key.find(".") > -1):#package
                data = data.replace(bytes(key, "utf-8"), bytes(self.replace_dict[key], "utf-8"))
            else:#class or function
                pass
                pattern = re.compile(bytes("\W", "utf-8") + bytes(key, "utf-8"))
                mathes_list = pattern.findall(data)
                if(mathes_list):
                    unique_mathes = set(mathes_list)
                    for match in unique_mathes:
                        initial_char = chr(match[0])
                        data = data.replace(match, bytes(initial_char + self.replace_dict[key], "utf-8"))
        data = self.compressor.compressSwfData(data)
        return data

#==================================================================================================
#==================================================================================================

def obfuscateSwfFiles(path):
    folder_contents = os.listdir(path)
    for item_name in folder_contents:
        item_path = path + "\\" + item_name
        if(os.path.isdir(item_path)):
            obfuscateSwfFiles(item_path)
        elif(item_path.endswith(r".swf")):
            obf_data = obfuscator.obfuscateSWF(item_path)
            obf_file_path = item_path.replace(swfs_root_path, obf_swfs_path)
            file_manager.saveFile(obf_data, obf_file_path, "wb+")

#get code path used in swf
core_root_path = input("[+] Code root path: ")

#create REPLACE MAP
map_creator = ObfuscationMapCreator()
replace_map_path = map_creator.create(core_root_path)

#create obfuscator
obfuscator = Obfuscator(replace_map_path)

#prepare folders for obf swfs
swfs_root_path = input("[+] SWFs root path: ")
folder_mapper = FolderMapper(swfs_root_path)
temp_list = swfs_root_path.split("\\")
temp_list.pop()
obf_swfs_path = "\\".join(temp_list) + "\OBFUSCATED"
dirs_to_create = folder_mapper.getLeafPathList(obf_swfs_path)
file_manager = FileManager()
file_manager.makeDirectoryTrees(dirs_to_create)

#start obfuscation
obfuscateSwfFiles(swfs_root_path)

print("[*] Complete!")
input("")